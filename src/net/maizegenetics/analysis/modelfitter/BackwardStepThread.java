package net.maizegenetics.analysis.modelfitter;

import java.util.ArrayList;

import net.maizegenetics.analysis.modelfitter.StepwiseOLSModelFitter.MODEL_TYPE;
import net.maizegenetics.dna.snp.GenotypeTable;
import net.maizegenetics.stats.linearmodels.LinearModelUtils;
import net.maizegenetics.stats.linearmodels.ModelEffect;
import net.maizegenetics.stats.linearmodels.SweepFastLinearModel;

/**
 * @author William Metcalf, bulk of code modified from original
 *         StepwiseOLSModelFitter.java
 *         <p>
 *         Class which implements Runnable designed to break up the work in the
 *         forward step of the OLSModelFitter class. The OLSModelFitter object
 *         instantiates MAX_THREAD_COUNT of this class near the beginning of the
 *         forwardStep()
 *
 */
public class BackwardStepThread implements Runnable{
	
	private StepwiseOLSModelFitter model;
	private int threadNumber;
	private int numberOfBaseModelEffects;
	private int startRange;
	private int endRange;
	private ArrayList<ModelEffect> currentModel;
	private GenotypeTable myGenotype;
	private int stepCount;
	private int maxThreadsNeeded;
	
	public BackwardStepThread(StepwiseOLSModelFitter model, int numberOfBaseModelEffects, int maxThreads, int threadNumber){
		this.model = model;
		this.threadNumber = threadNumber;
		this.numberOfBaseModelEffects = numberOfBaseModelEffects;
		this.maxThreadsNeeded = maxThreads;
		currentModel = new ArrayList<ModelEffect>();
		for (ModelEffect me : model.getCurrentModel()) {
			currentModel.add(me);
		}
		myGenotype = model.myGenotype;
		determineRanges();
	}

	@Override
	public void run() {
		double bestbic = Double.MAX_VALUE;
		double bestmbic = Double.MAX_VALUE;
		double bestaic = Double.MAX_VALUE;

		int n = model.getY().length;
		int numberOfSites = myGenotype.numberOfSites();

		SweepFastLinearModel sflm0 = new SweepFastLinearModel(currentModel, model.getY());

		//find the model term (snps only) with the largest p-value
		double maxp = 0;
		double minF= -1;
		int maxterm = 0;
		ModelEffect worstMarkerEffect = null;
		double[] errorssdf = sflm0.getResidualSSdf();
		
		for (int t = startRange; t < endRange; t++) {
			if (model.isCountSteps())
				stepCount++;
			double bic;
			double mbic;
			double aic;
			ModelEffect markerEffect = null;
			double[] termssdf = sflm0.getIncrementalSSdf(t);
			double F = termssdf[0]/termssdf[1]/errorssdf[0]*errorssdf[1];
			double p;
			if(model.getModelType() != MODEL_TYPE.pvalue){

				ModelEffect meTest1 = currentModel.remove(t);
				SNP snpRemoved = (SNP) meTest1.getID(); 
				System.out.println("CORRECT The SNP just removed is: " + snpRemoved);

				SweepFastLinearModel sflm = new SweepFastLinearModel(currentModel, model.getY()); 
				//Calculate the BIC
				double [] errorss = sflm.getResidualSSdf();
				double [] modeldf = sflm.getFullModelSSdf();
				double pForBIC  = modeldf[1]; 
				bic = (n*Math.log(errorss[0]/n)) + (pForBIC*Math.log(n)) ; 
				aic = (n*Math.log(errorss[0]/n)) + (2*pForBIC) ;

				//Calculate the mBIC
				double numberOfTwoWayInteractions = (double) (numberOfSites*(numberOfSites-1))/2;			
				double pForMbic = modeldf[1];
				double qForMbic = 0; //This is going to be the number of two-way interaction terms. For now, this is not implemented, and thus I am setting it equal to zer
				if(qForMbic == 0){
					mbic = (n*Math.log(errorss[0])) + ((pForMbic+qForMbic)*Math.log(n)) + (2*pForMbic*(Math.log((numberOfSites/2.2)-1)));
				}else{
					mbic = (n*Math.log(errorss[0])) + ((pForMbic+qForMbic)*Math.log(n)) + (2*pForMbic*(Math.log((numberOfSites/2.2)-1))) + 
							(2*qForMbic*(Math.log((numberOfTwoWayInteractions/2.2)-1) )); 
				}


				currentModel.add(t, meTest1);

				sflm = new SweepFastLinearModel(currentModel, model.getY()); 


			} else{
				bic = Double.MAX_VALUE;
				mbic = Double.MAX_VALUE;
				aic = Double.MAX_VALUE;
			}

			try{
				p = LinearModelUtils.Ftest(F, termssdf[1], errorssdf[1]);  
			} catch (Exception e){
				p = Double.NaN;
			}


			switch(model.getModelType()){
			case pvalue:
				try {
					if (p > maxp) {
						maxterm = t;
						maxp = p;
						minF = F; 
					}
				} catch(Exception e){

				}
				break;
			case bic:
				if(bic < bestbic){
					bestbic = bic;
					bestaic = aic;
					bestmbic = mbic;
					maxterm = t;  //Actually, this should be "minterm" because we are finding the model with the minimum BIC, but I am keeping this as "maxterm" for simpilicity of the calculations
					worstMarkerEffect = markerEffect;
					maxp = p;
					minF = F; 
				}
				break;
			case mbic:
				if(mbic < bestmbic){
					bestbic = bic;
					bestaic = aic;
					bestmbic = mbic;
					maxterm = t;
					worstMarkerEffect = markerEffect;
					maxp = p;
					minF = F;
				}
				break;
			case aic:
				if(aic < bestaic){
					bestbic = bic;
					bestaic = aic;
					bestmbic = mbic;
					maxterm = t;
					worstMarkerEffect = markerEffect;
					maxp = p;
					minF = F;
				}
				break;
			} 

		}
		
		model.threadListenerBackwardStep(bestbic, bestaic, bestmbic, maxterm, worstMarkerEffect, maxp, minF, threadNumber, stepCount);
		
	}
	
	/**
	 * Determines the startRange and endRage for this particular thread worker.
	 * Since it isn't guaranteed that each thread will do the same amount of
	 * work, this is set up to give all the threads but one an equal amount, and
	 * then put all of the extra on the last thread. The start range is
	 * inclusive, while the end range is exclusive.
	 * <p>
	 * For example, if there are 1106 markers and 8 threads, threads 0 - 6 will
	 * have 138 markers each, and thread 7 will have 140.
	 */
	private void determineRanges() {
		if (maxThreadsNeeded == 1){
			startRange = 0;
			endRange = numberOfBaseModelEffects;
		}
		int baseSize = numberOfBaseModelEffects / maxThreadsNeeded;
		startRange = threadNumber * baseSize;
		endRange = (threadNumber + 1) * baseSize;
		// since the number of sites might not be a multiple of the
		// MAX_THREAD_COUNT
		// currently just puts all the extras on the last thread (wont be more
		// than maxThreadCount - 1 extra)
		if (threadNumber == maxThreadsNeeded - 1)
			endRange = model.myGenotype.numberOfSites();
	}

}
