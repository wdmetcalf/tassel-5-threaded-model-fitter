package net.maizegenetics.analysis.modelfitter;

import java.util.ArrayList;

import net.maizegenetics.stats.linearmodels.ModelEffect;
import net.maizegenetics.stats.linearmodels.PartitionedLinearModel;
import net.maizegenetics.stats.linearmodels.SweepFastLinearModel;

/**
 * @author William Metcalf
 * <p>Wrapper class to hold information about the Epistatic Effect
 */
public class EpistaticEffect {
	private int[] markerNumbers;
	private SNP[] SNPNumbers = new SNP[2];
	private ModelEffect effect = null;
	private double modelss = Double.MIN_VALUE;
	private double bic = Double.MAX_VALUE;
	private double mbic = Double.MAX_VALUE;
	private double aic = Double.MAX_VALUE;
	
	public EpistaticEffect(int epi1, int epi2){
		markerNumbers = new int[]{epi1, epi2};
	}
	
	/**
	 * Compares this EpistaticEffect to another
	 * @param ef - another EpistaticEffect to compare to
	 * @param model - the main StepwiseOLSModelFitter
	 * @return the better of the two EpistaticEffect objects, determined based on 
	 * the models ModelType
	 */
	public EpistaticEffect compareTo (EpistaticEffect ef, StepwiseOLSModelFitter model){
		
		//create a copy of the current model
		ArrayList<ModelEffect> currentModel = new ArrayList<ModelEffect>();
		for (ModelEffect me : model.getCurrentModel()){
			currentModel.add(me);
		}
		
		//choose which effect is better
		//declare variables used in multiple cases
		double[] modeldf;
		int n;
		SweepFastLinearModel sflm2;
		double[] errorss;
		double pForBIC;
		
		
		switch(model.getModelType()){
		case pvalue:
			SweepFastLinearModel sflm = new SweepFastLinearModel(currentModel,
					model.getY());
			PartitionedLinearModel plm = new PartitionedLinearModel(currentModel,
					sflm);
			plm.testNewModelEffect(ef.getModelEffect());
			double modelss = plm.getModelSS();
			if (modelss > this.modelss)
				return ef;
			else 
				return this;
		case bic:
			sflm2 = new SweepFastLinearModel(currentModel, model.getY());
			n = model.numberNotMissing;
			errorss = sflm2.getResidualSSdf();
			modeldf = sflm2.getFullModelSSdf();
			pForBIC = modeldf[1];
			double bic = (n * Math.log(errorss[0] / n))
					+ (pForBIC * Math.log(n));
			if (bic < this.bic)
				return ef;
			else
				return this;
		case mbic:
			double mbic;
			sflm2 = new SweepFastLinearModel(currentModel, model.getY());
			modeldf = sflm2.getFullModelSSdf();
			errorss = sflm2.getResidualSSdf();
			double pForMbic = modeldf[1];
			double qForMbic = 0;
			n = model.numberNotMissing;
			int numberOfSites = model.myGenotype.numberOfSites();
			int numberOfTwoWayInteractions = (n*(n-1))/2;
			if (qForMbic == 0) {
				mbic = (n * Math.log(errorss[0]))
						+ ((pForMbic + qForMbic) * Math.log(n))
						+ (2 * pForMbic * (Math
								.log((numberOfSites / 2.2) - 1)));
			} else {
				mbic = (n * Math.log(errorss[0]))
						+ ((pForMbic + qForMbic) * Math.log(n))
						+ (2 * pForMbic * (Math
								.log((numberOfSites / 2.2) - 1)))
						+ (2 * qForMbic * (Math
								.log((numberOfTwoWayInteractions / 2.2) - 1)));
			}
			if (mbic < this.mbic)
				return ef;
			else
				return this;
		case aic:
			n = model.numberNotMissing;
			sflm2 = new SweepFastLinearModel(currentModel, model.getY());
			errorss = sflm2.getResidualSSdf();
			modeldf = sflm2.getFullModelSSdf();
			pForBIC = modeldf[1];
			double aic = (n * Math.log(errorss[0] / n)) + (2 * pForBIC);
			if (aic < this.aic)
				return ef;
			else
				return this;
			
		default:
			//this should never happen
			throw new Error("Unknown model type");
		}
		
		
		
	}
	
	public void setModelEffect(ModelEffect effect){
		this.effect = effect;
	}
	
	public ModelEffect getModelEffect(){
		return this.effect;
	}

	public double getModelss() {
		return modelss;
	}

	public void setModelss(double modelss) {
		this.modelss = modelss;
	}

	public double getBic() {
		return bic;
	}

	public void setBic(double bic) {
		this.bic = bic;
	}

	public double getMbic() {
		return mbic;
	}

	public void setMbic(double mbic) {
		this.mbic = mbic;
	}

	public double getAic() {
		return aic;
	}

	public void setAic(double aic) {
		this.aic = aic;
	}

	public void setMarkerNumbers(int[] markerNumbers) {
		this.markerNumbers = markerNumbers;
	}

}
