package net.maizegenetics.analysis.modelfitter;

import java.util.ArrayList;

import net.maizegenetics.analysis.association.AssociationUtils;
import net.maizegenetics.dna.snp.GenotypeTable;
import net.maizegenetics.dna.snp.GenotypeTableUtils;
import net.maizegenetics.stats.linearmodels.CovariateModelEffect;
import net.maizegenetics.stats.linearmodels.ModelEffect;
import net.maizegenetics.stats.linearmodels.NestedCovariateModelEffect;
import net.maizegenetics.stats.linearmodels.PartitionedLinearModel;
import net.maizegenetics.stats.linearmodels.SweepFastLinearModel;

/**
 * @author William Metcalf, bulk of code modified from original
 *         StepwiseOLSModelFitter.java
 *         <p>
 *         Class which implements Runnable designed to break up the work in the
 *         forward step of the OLSModelFitter class. The OLSModelFitter object
 *         instantiates MAX_THREAD_COUNT of this class near the beginning of the
 *         forwardStep()
 *
 */
public class ForwardStepThread implements Runnable {
	private StepwiseOLSModelFitter model;
	private int threadNumber;
	private int startRange;
	private int endRange;
	private ArrayList<ModelEffect> currentModel;
	private GenotypeTable myGenotype;
	// for testing purposes
	private int stepCount = 0;
	private int maxThreadsNeeded;
	private double bestss = 0;
	private double bestbic = Double.MAX_VALUE;
	private double bestmbic = Double.MAX_VALUE;
	private double bestaic = Double.MAX_VALUE;
	private ModelEffect besteffect = null;

	public ForwardStepThread(StepwiseOLSModelFitter model, int i, int maxThreadsNeeded) {
		this.model = model;
		threadNumber = i;
		this.maxThreadsNeeded = maxThreadsNeeded;

		// create a copy of the current working model
		// this has to be a full new physical copy,
		// since otherwise each thread would be attempting to modify it
		// causing bad (ConcurrentModification) errors
		currentModel = new ArrayList<ModelEffect>();
		for (ModelEffect me : model.getCurrentModel()) {
			currentModel.add(me);
		}

		// this isn't a full physical copy, just a pointer to the
		// original myGenotype in the StepwiseOLSModelFitter class.
		// I made a local variable for it since it is used frequently, and
		// I'd rather not use the getter function for it that often.
		myGenotype = model.myGenotype;
		determineRanges();

	}

	@Override
	public void run() {
		// NOTE: these aren't going to be the global bests, just the best
		// from this thread's chunk. Will later be compared to the best
		// results from the other threads.
		
		ArrayList<ModelEffect> currentModel = new ArrayList<ModelEffect>();
		for (ModelEffect me : model.getCurrentModel())
			currentModel.add(me);
				
		for (int s = startRange; s < endRange; s++) {
			if (model.isCountSteps())
				stepCount++;
			if (!model.getExcludedSNPs().contains(s)) {
				// create the appropriate marker effect
				ModelEffect markerEffect = null;
				SNP snp = new SNP(myGenotype.siteName(s),
						myGenotype.chromosome(s),
						myGenotype.chromosomalPosition(s), s);

				// if the Genotype has a reference allele use that, else if
				// genotype use that else nothing, allele probability not
				// implemented
				// for genotype use an additive model for now
				if (myGenotype.hasReferenceProbablity()) {
					double[] cov = new double[model.numberNotMissing];
					int count = 0;
					for (int i = 0; i < model.totalNumber; i++) {
						if (!model.getMissing().fastGet(i))
							cov[count++] = myGenotype
									.referenceProbability(i, s);
					}

					if (model.isNested()) {
						CovariateModelEffect cme = new CovariateModelEffect(cov);
						markerEffect = new NestedCovariateModelEffect(cme,
								model.getNestingEffect());
						markerEffect.setID(snp);
					} else {
						markerEffect = new CovariateModelEffect(cov, snp);
					}
				} else if (myGenotype.hasGenotype()) {
					byte minor = myGenotype.minorAllele(s);
					double[] cov = new double[model.numberNotMissing];
					byte[] siteGeno = AssociationUtils.getNonMissingBytes(
							myGenotype.genotypeAllTaxa(s), model.getMissing());
					for (int i = 0; i < model.numberNotMissing; i++) {
						byte[] diploidAlleles = GenotypeTableUtils
								.getDiploidValues(siteGeno[i]);
						if (diploidAlleles[0] == minor)
							cov[i] += 0.5;
						if (diploidAlleles[0] == minor)
							cov[i] += 0.5;
					}

					if (model.isNested()) {
						CovariateModelEffect cme = new CovariateModelEffect(cov);
						markerEffect = new NestedCovariateModelEffect(cme,
								model.getNestingEffect());
						markerEffect.setID(snp);
					} else {
						markerEffect = new CovariateModelEffect(cov, snp);
					}

				} else {
					throw new IllegalArgumentException(
							"No genotypes or reference probabilities in the data set.");
				}
				
				//determine if this effect is the best one yet
				double value = model.getMarkerComparisonValue(markerEffect, currentModel);
				
				switch(model.getModelType()){
				case pvalue:
					if (value > this.bestss){
						this.bestss = value;
						this.besteffect = markerEffect;
					}
					break;
				case bic:
					if (value < this.bestbic){
						this.bestbic = value;
						this.besteffect = markerEffect;
					}
					break;
				case mbic:
					if (value < this.bestmbic){
						this.bestmbic = value;
						this.besteffect = markerEffect;
					}
					break;
				case aic:
					if (value < this.bestaic){
						this.bestaic = value;
						this.besteffect = markerEffect;
					}
					break;
					
				}
			}
		}
		
		AdditiveEffect bestFoundEffect = new AdditiveEffect();
		bestFoundEffect.setAic(bestaic);
		bestFoundEffect.setMbic(bestmbic);
		bestFoundEffect.setBic(bestbic);
		bestFoundEffect.setModelss(bestss);
		bestFoundEffect.setEffect(besteffect);
		model.threadListenerForwardStep(threadNumber, bestFoundEffect, stepCount);
	}

	/**
	 * Determines the startRange and endRage for this particular thread worker.
	 * Since it isn't guaranteed that each thread will do the same amount of
	 * work, this is set up to give all the threads but one an equal amount, and
	 * then put all of the extra on the last thread. The start range is
	 * inclusive, while the end range is exclusive.
	 * <p>
	 * For example, if there are 1106 markers and 8 threads, threads 0 - 6 will
	 * have 138 markers each, and thread 7 will have 140.
	 */
	private void determineRanges() {
		if (maxThreadsNeeded == 1){
			startRange = 0;
			endRange = model.myGenotype.numberOfSites();
			return;
		}
		int baseSize = model.myGenotype.numberOfSites() / maxThreadsNeeded;
		startRange = threadNumber * baseSize;
		endRange = (threadNumber + 1) * baseSize;
		// since the number of sites might not be a multiple of the
		// MAX_THREAD_COUNT
		// currently just puts all the extras on the last thread (wont be more
		// than maxThreadCount - 1 extra)
		if (threadNumber == maxThreadsNeeded - 1)
			endRange = model.myGenotype.numberOfSites();
	}

}
