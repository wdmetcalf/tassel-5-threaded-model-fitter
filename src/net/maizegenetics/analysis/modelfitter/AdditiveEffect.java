package net.maizegenetics.analysis.modelfitter;

import java.util.ArrayList;

import net.maizegenetics.stats.linearmodels.ModelEffect;

public class AdditiveEffect {
	protected ModelEffect effect;
	protected double modelss;
	protected double bic;
	protected double mbic;
	protected double aic;
	private int[] epiMarkerNumbers;
	
	public AdditiveEffect(){
		modelss = 0;
		bic = Double.MAX_VALUE;
		mbic = Double.MAX_VALUE;
		aic = Double.MAX_VALUE;
		epiMarkerNumbers = new int[2];
	}
	
	public void updateComparisonValue(StepwiseOLSModelFitter model, ArrayList<ModelEffect> currentModel){
		switch(model.getModelType()){
		case pvalue:
			modelss = model.getMarkerComparisonValue(effect, currentModel);
			break;
		case mbic:
			mbic = model.getMarkerComparisonValue(effect, currentModel);
			break;
		case bic:
			bic = model.getMarkerComparisonValue(effect, currentModel);
			break;
		case aic:
			aic = model.getMarkerComparisonValue(effect, currentModel);
			break;
		}
	}
	
	public void setEpiMarkerNumbers(int[] n){
		this.epiMarkerNumbers = n;
	}
	
	public String getEpiMarkerNumbersToString(){
		String s = "";
		s += "(" + epiMarkerNumbers[0] + ", " + epiMarkerNumbers[1] + ")";
		
		return s;
	}
	
	
	public ModelEffect getEffect() {
		return effect;
	}

	public void setEffect(ModelEffect effect) {
		this.effect = effect;
	}

	public double getModelss() {
		return modelss;
	}

	public void setModelss(double modelss) {
		this.modelss = modelss;
	}

	public double getBic() {
		return bic;
	}

	public void setBic(double bic) {
		this.bic = bic;
	}

	public double getMbic() {
		return mbic;
	}

	public void setMbic(double mbic) {
		this.mbic = mbic;
	}

	public double getAic() {
		return aic;
	}

	public void setAic(double aic) {
		this.aic = aic;
	}

	public int[] getEpiMarkerNumbers() {
		// TODO Auto-generated method stub
		return this.epiMarkerNumbers;
	}
	
}
