package net.maizegenetics.analysis.modelfitter;

import java.util.ArrayList;

import net.maizegenetics.analysis.association.AssociationUtils;
import net.maizegenetics.dna.snp.GenotypeTable;
import net.maizegenetics.dna.snp.GenotypeTableUtils;
import net.maizegenetics.stats.linearmodels.CovariateModelEffect;
import net.maizegenetics.stats.linearmodels.ModelEffect;
import net.maizegenetics.stats.linearmodels.NestedCovariateModelEffect;
import net.maizegenetics.stats.linearmodels.PartitionedLinearModel;
import net.maizegenetics.stats.linearmodels.SweepFastLinearModel;

/**
 * @author William Metcalf and Alex Lipka
 *         <p>
 */
public class EpistasisThread implements Runnable {
	private int threadNumber;
	private int threadCount;
	private int position;
	private StepwiseOLSModelFitter model;
	private GenotypeTable myGenotype; // just for ease of use
	private int numberOfSites; // just for ease of use
	private EpistaticEffect bestEpistaticEffect;

	public EpistasisThread(int threadNumber, int threadCount,
			StepwiseOLSModelFitter model) {
		this.threadNumber = threadNumber;
		this.threadCount = threadCount;
		this.model = model;
		this.myGenotype = model.myGenotype;
		this.numberOfSites = myGenotype.numberOfSites();
	}

	@Override
	public void run() {
		/*
		 * This thread class doesn't break the load into chunks of equal size,
		 * since the earlier markers require more work. Instead, the thread will
		 * start at its threadNumber position, then do every threadCount'th
		 * marker. For example, if there are 1000 markers (0-999) and 8 threads
		 * (0-7), thread 3 will start at marker 3, and continue to marker 11,
		 * 19, 27, 35, etc., until it reaches the end.
		 */

		int marker1 = threadNumber;
		AdditiveEffect bestEffect = new AdditiveEffect();
		
		ArrayList<ModelEffect> currentModel = new ArrayList<ModelEffect>();
		for (ModelEffect me : model.getCurrentModel())
			currentModel.add(me);

		while (marker1 < numberOfSites - 1) {
			SNP snp = new SNP(myGenotype.siteName(marker1),
					myGenotype.chromosome(marker1),
					myGenotype.chromosomalPosition(marker1), marker1);
			for (int marker2 = marker1 + 1; marker2 < numberOfSites; marker2++) {
				// create the model effect
				ModelEffect markerEffect = null;
				SNP snpEpi = new SNP(myGenotype.siteName(marker2),
						myGenotype.chromosome(marker2),
						myGenotype.chromosomalPosition(marker2), marker2);
//				String EpiEffectName = snp.toString() + "x" + snpEpi.toString()
//						+ "(" + marker1 + ", " + marker2 + ")";

				// if the Genotype has reference allele use that, else if
				// genotype use that else nothing, allele probability not
				// implemented for
				// genotype use an additive model for now
				if (myGenotype.hasReferenceProbablity()) {
					double[] cov = new double[model.numberNotMissing];
					int count = 0;
					for (int i = 0; i < model.totalNumber; i++) {
						if (!model.getMissing().fastGet(i))
							cov[count++] = myGenotype.referenceProbability(i,
									marker1)
									* myGenotype.referenceProbability(i,
											marker2);
					}

					if (model.isNested()) {
						CovariateModelEffect cme = new CovariateModelEffect(cov);
						markerEffect = new NestedCovariateModelEffect(cme,
								model.getNestingEffect());
						markerEffect.setID(snp);
						markerEffect.setID2(snpEpi);
					} else {
						markerEffect = new CovariateModelEffect(cov);
						markerEffect.setID(snp);
						markerEffect.setID2(snpEpi);
					}
				} else if (myGenotype.hasGenotype()) {
					byte minor = myGenotype.minorAllele(marker1);
					double[] cov = new double[model.numberNotMissing];
					double[] covEpi = new double[model.numberNotMissing];
					byte[] siteGeno = AssociationUtils.getNonMissingBytes(
							myGenotype.genotypeAllTaxa(marker1),
							model.getMissing());
					byte[] siteGenoEpi = AssociationUtils.getNonMissingBytes(
							myGenotype.genotypeAllTaxa(marker2),
							model.getMissing());

					// #TODO: merge these repeat lines?
					for (int i = 0; i < model.numberNotMissing; i++) {
						byte[] diploidAlleles = GenotypeTableUtils
								.getDiploidValues(siteGeno[i]);
						byte[] diploidAllelesEpi = GenotypeTableUtils
								.getDiploidValues(siteGenoEpi[i]);
						if (diploidAlleles[0] == minor)
							cov[i] += 0.5;
						if (diploidAlleles[0] == minor)
							cov[i] += 0.5;
						if (diploidAllelesEpi[0] == minor)
							covEpi[i] += 0.5;
						if (diploidAllelesEpi[0] == minor)
							covEpi[i] += 0.5;
					}

					// Multiply all of the elements of "cov" by the elements of
					// "covEpi" - this will create the approrpiate
					// explanatory variables for a two-way interaction term
					for (int idx = 0; idx < cov.length; idx++) {
						cov[idx] = cov[idx] * covEpi[idx];
					}

					if (model.isNested()) {
						CovariateModelEffect cme = new CovariateModelEffect(cov);
						markerEffect = new NestedCovariateModelEffect(cme,
								model.getNestingEffect());
						markerEffect.setID(snp);
						markerEffect.setID2(snpEpi);
					} else {
						markerEffect = new CovariateModelEffect(cov);
						markerEffect.setID(snp);
						markerEffect.setID2(snpEpi);
					}

				} else {
					throw new IllegalArgumentException(
							"No genotypes or reference probabilities in the data set.");
				} // end create marker effect

				AdditiveEffect epiEffect = new AdditiveEffect();
				epiEffect.setEffect(markerEffect);
				epiEffect.updateComparisonValue(model, currentModel);
				epiEffect.setEpiMarkerNumbers(new int[] { marker1, marker2 });

				bestEffect = model.compareEffects(bestEffect, epiEffect);
			}
			marker1 += this.threadCount;

//			System.out.printf("Thread #%d: Step #%d\t current markers: %d, %d\n",
//					threadNumber, marker1, bestEffect.getEpiMarkerNumbers()[0],
//					bestEffect.getEpiMarkerNumbers()[1]);
		}
		model.EpistasisThreadListener(threadNumber, bestEffect);
//		System.out.printf("Thead #%d completed", threadNumber);

	}

}
